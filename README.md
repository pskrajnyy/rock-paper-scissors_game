# Project info
Classic rock paper scissors console game implemented in Java. In order to win we have to defeat computer required amount of times, which has been set in game settings (default value is three).
Computer randomly chooses one of the possible moves (Rock, Paper, Scissors, Lizard or Spock). 
The logic of the game is set so that the chances of a draw and losing a computer are 25%, and the chances of winning - 50%.

# Project insight
![App](/misc/app-screen.png)