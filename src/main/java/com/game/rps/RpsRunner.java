package com.game.rps;

import java.util.InputMismatchException;
import java.util.Scanner;

public class RpsRunner {
    private static boolean end = false;
    private static int round;
    private static String username;
    private static int winRoundsUser;
    private static int winRoundsComputer;
    private static Figure figureUser;
    private static Figure figureComputer;

    public static void main(String[] args) {
        while(true) {
            menu();
            controlsInfo();
            while (!end) {
                figureUser = ControlsByUser.runUser();
                figureComputer = ComputerRun.runComputer(figureUser);
                resultInformation();
            }
        }
    }

    static void setWinRoundsUser(int winRoundsUser) {
        RpsRunner.winRoundsUser = winRoundsUser;
    }

    static void setWinRoundsComputer(int winRoundsComputer) {
        RpsRunner.winRoundsComputer = winRoundsComputer;
    }

    static int getWinRoundsUser() {
        return winRoundsUser;
    }

    static int getWinRoundsComputer() {
        return winRoundsComputer;
    }

    private static void menu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Gra w rozbudowana wersje gry kamien, papier, nozyce" +
                "\nPodaj swoje imie: ");
        username = scanner.nextLine();
        System.out.println("Podaj liczbe wygranych rund po ktorych nastepuje zwyciestwo: ");
        try {
            round = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Prosze o podanie liczby");
            System.exit(0);
        }
        end = false;
    }

    private static void controlsInfo() {
        System.out.println("Informacje o sterowaniu");
        System.out.println("*klawisz 1 - zagranie 'kamien'" +
                "\n*klawisz 2 - zagranie 'papier'" +
                "\n*klawisz 3 - zagranie 'nozyce'" +
                "\n*klawisz 4 - zagranie 'jaszczurka'" +
                "\n*klawisz 5 - zagranie 'Spock'" +
                "\n*klawisz x - zakonczenie gry" +
                "\n*klawisz n - restart gry" +
                "\n*******************************");
    }

    private static void resultInformation() {
        System.out.println("Zagrales: " + figureUser + " \nKomputer: " + figureComputer);
        System.out.println("Obecny wynik rund jest nastepujacy: " +
                "\n Komputer: " + winRoundsComputer + " " + username + ": " +winRoundsUser +
                "\n**********************************************");
        if(winRoundsUser == round) {
            System.out.println("Gratulacje!! wygrales!");
            ControlsByUser.restartOrExit();
            end = true;
        } else if(winRoundsComputer == round) {
            System.out.println("Pregrales ;(");
            ControlsByUser.restartOrExit();
            end = true;
        }
    }
}
