package com.game.rps;

import java.util.Random;

class ComputerRun {

    static Figure runComputer(Figure userChoose) {
        Random random = new Random();
        int chooseComputer = random.nextInt(101);
        Figure figureComputer = null;
        if(chooseComputer <= 50) {
            figureComputer = computerMustWin(userChoose);
        } else if (chooseComputer <= 75) {
            figureComputer = userChoose;
        } else if (chooseComputer <= 100) {
            figureComputer = computerMustLose(userChoose);
        }
        return figureComputer;
    }

    private static Figure computerMustLose(Figure userChoose) {
        Figure figureComputer = null;
        Random rand = new Random(2);
        int chooseFigure = rand.nextInt();
        switch (userChoose) {
            case papier: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.kamien;
                } else {
                    figureComputer = Figure.Spock;
                }
                break;
            }
            case kamien: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.jaszczurka;
                } else {
                    figureComputer = Figure.nozyczki;
                }
                break;
            }
            case Spock: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.kamien;
                } else {
                    figureComputer = Figure.nozyczki;
                }
                break;
            }
            case nozyczki: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.papier;
                } else {
                    figureComputer = Figure.jaszczurka;
                }
                break;
            }
            case jaszczurka: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.Spock;
                } else {
                    figureComputer = Figure.nozyczki;
                }
                break;
            }
        }

        RpsRunner.setWinRoundsUser(RpsRunner.getWinRoundsUser()+1);
        return figureComputer;
    }

    private static Figure computerMustWin(Figure userChoose) {
        Figure figureComputer = null;
        Random rand = new Random(2);
        int chooseFigure = rand.nextInt();

        switch (userChoose) {
            case papier: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.jaszczurka;
                } else {
                    figureComputer = Figure.nozyczki;
                }
                break;
            }
            case kamien: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.papier;
                } else {
                    figureComputer = Figure.Spock;
                }
                break;
            }
            case Spock: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.jaszczurka;
                } else {
                    figureComputer = Figure.papier;
                }
                break;
            }
            case nozyczki: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.kamien;
                } else {
                    figureComputer = Figure.Spock;
                }
                break;
            }
            case jaszczurka: {
                if(chooseFigure == 0) {
                    figureComputer = Figure.kamien;
                } else {
                    figureComputer = Figure.nozyczki;
                }
                break;
            }
        }
        RpsRunner.setWinRoundsComputer(RpsRunner.getWinRoundsComputer()+1);
        return figureComputer;
    }
}
