package com.game.rps;

import java.util.Scanner;

class ControlsByUser {

    private static void restart() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Na pewno chcesz zaczac gre od nowa? T lub N");
        String answer = scanner.nextLine();
        if(answer.equals("T") || answer.equals("t")) {
            RpsRunner.setWinRoundsComputer(0);
            RpsRunner.setWinRoundsUser(0);
            String[] args = new String[0];
            RpsRunner.main(args);
        }
    }

    static void restartOrExit() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Chcesz ponownie zagrac czy wyjsc?");
        System.out.println("T - restart, N - wyjscie");
        String choose = scanner.next();
        if(choose.equals("t") || choose.equals("T")) {
            for(int i=0; i<10; i++) {
                System.out.println(" ");
            }
        } else if (choose.equals("n") || choose.equals("N")) {
            exit();
        } else {
            System.out.println("Podałeś niepoprawny klawisz");
            exit();
        }
    }

    static Figure runUser() {
        Scanner scanner = new Scanner(System.in);
        Figure chooseUser = null;
        boolean correctChoose = false;
        while(!correctChoose) {
            System.out.println("Podaj swoj ruch " +
                    "\n1. Kamien 2. Papier 3. Nozyce 4. Jaszczurka 5. Spock" +
                    "\nx - wyjscie n - restart");
            String choose = scanner.next();
            switch (choose) {
                case "1": {
                    chooseUser = Figure.kamien;
                    correctChoose = true;
                    break;
                }
                case "2": {
                    chooseUser = Figure.papier;
                    correctChoose = true;
                    break;
                }
                case "3": {
                    chooseUser = Figure.nozyczki;
                    correctChoose = true;
                    break;
                }
                case "4": {
                    chooseUser = Figure.jaszczurka;
                    correctChoose = true;
                    break;
                }
                case "5": {
                    chooseUser = Figure.Spock;
                    correctChoose = true;
                    break;
                }
                case "x": {
                    exit();
                    break;
                }
                case "n": {
                    correctChoose = true;
                    restart();
                    break;
                }
                default: {
                    correctChoose = false;
                    System.out.println("Podales zly znak, sprobuj ponownie" +
                            "\n*************************");
                    break;
                }
            }
        }
        return chooseUser;
    }

    private static void exit() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Na pewno chcesz zakonczyc gre? T lub N");
        String answer = scanner.nextLine();
        if(answer.equals("T") || answer.equals("t")) {
            System.exit(0);
        }
    }
}
